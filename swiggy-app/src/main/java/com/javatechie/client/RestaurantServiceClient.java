package com.javatechie.client;

import com.javatechie.dto.OrderResponseDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Component
public class RestaurantServiceClient {
    @Autowired
    private RestTemplate template;
    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    public OrderResponseDTO fetchOrderStatus(String orderId) {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cnVuZ25kIiwiaWF0IjoxNzAwNzA5MTgyLCJleHAiOjE3MDA3MTI3ODJ9.BIm-1I3dBPKd-33pOK9t1KtEWB1IvOQ778blLA41w04");

        HttpEntity<String> request = new HttpEntity<String>(headers);
        return circuitBreaker.run(() ->
                        template.exchange("http://RESTAURANT-SERVICE/restaurant/orders/status/" + orderId, HttpMethod.GET, request, OrderResponseDTO.class).getBody()
                , throwable -> getDefaultAlbumList());

    }

    OrderResponseDTO getDefaultAlbumList() {
        return new OrderResponseDTO();
    }

}
