package com.javatechie.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiResponse<T> {
    private String messageCode;
    private String message;
    private T data;
}
