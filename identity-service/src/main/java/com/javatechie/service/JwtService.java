package com.javatechie.service;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtService {
//    public static final String SECRET = "ngodactrungfromthuanthanhbacninh99f1manhanvien428992";
    public static final String SECRET = "bmdvZGFjdHJ1bmdmcm9tdGh1YW50aGFuaGJhY25pbmg5OWYxbWFuaGFudmllbjQyODk5Mg==";

    // JWT expiration time (e.g., 1 hour)
    long expirationTime = 3600000; // 1 hour in milliseconds

    // Current time
    long currentTimeMillis = System.currentTimeMillis();

    public void validateToken(String authToken) {
        try {
            Jwts.parserBuilder().setSigningKey(key()).build().parse(authToken);
        } catch (MalformedJwtException ex) {
            throw new RuntimeException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new RuntimeException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new RuntimeException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new RuntimeException("JWT claims string is empty.");
        }
    }


    public String generateToken(String userName) {
        return createToken(userName);
    }

    private String createToken(String userName) {
        return Jwts.builder()
                .setSubject(userName)
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + expirationTime))
                .signWith(key(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key key() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET));
    }

    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
