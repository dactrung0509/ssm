package com.javatechie.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean
    public RestTemplate template(){
//        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
//        interceptors.add(new HeaderRequestInterceptor("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cnVuZ25kIiwiaWF0IjoxNjk5OTU0ODE4LCJleHAiOjE2OTk5NTg0MTh9.58viSuANIPH-lpmCuPQAxX2oleMpgce8YPnB8wAiyRE"));
        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
